﻿namespace WebSale.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        private WebSaleDbContext _dbContext;
        public WebSaleDbContext Init => _dbContext ?? (_dbContext = WebSaleDbContext.Create());

        protected override void DisposeCore()
        {
            _dbContext?.Dispose();
        }
    }
}