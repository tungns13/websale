﻿using System.Threading.Tasks;

namespace WebSale.Data.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();

        Task CommitAsync();
    }
}