﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WebSale.Data.Infrastructure
{
    public abstract class RepositoryBase<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private WebSaleDbContext _dbContext;
        private readonly IDbSet<TEntity> _dbSet;

        protected IDbFactory DbFactory
        {
            get;
        }

        protected WebSaleDbContext DbContext => _dbContext ?? (_dbContext = DbFactory.Init);

        protected RepositoryBase(IDbFactory dbFactory)
        {
            DbFactory = dbFactory;
            _dbSet = DbContext.Set<TEntity>();
        }

        protected virtual IQueryable<TEntity> GetQueryable(
        List<Expression<Func<TEntity, bool>>> filters = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
        string includeProperties = null,
        int? skip = null,
        int? take = null)
        {
            includeProperties = includeProperties ?? string.Empty;
            IQueryable<TEntity> query = DbContext.Set<TEntity>();

            if (filters != null)
            {
                foreach (var filter in filters)
                {
                    query = query.Where(filter);
                }
            }

            query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            if (skip.HasValue)
            {
                query = query.Skip(skip.Value);
            }

            if (take.HasValue)
            {
                query = query.Take(take.Value);
            }

            return query;
        }

        public virtual IEnumerable<TEntity> GetAll(
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null,
            int? skip = null,
            int? take = null)
        {
            return GetQueryable(null, orderBy, includeProperties, skip, take).ToList();
        }

        public virtual async Task<IEnumerable<TEntity>> GetAllAsync(
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null,
            int? skip = null,
            int? take = null)
        {
            return await GetQueryable(null, orderBy, includeProperties, skip, take).ToListAsync();
        }

        public virtual IEnumerable<TEntity> Get(
            List<Expression<Func<TEntity, bool>>> filters = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null,
            int? skip = null,
            int? take = null)
        {
            return GetQueryable(filters, orderBy, includeProperties, skip, take).ToList();
        }

        public virtual async Task<IEnumerable<TEntity>> GetAsync(
            List<Expression<Func<TEntity, bool>>> filters = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null,
            int? skip = null,
            int? take = null)
        {
            return await GetQueryable(filters, orderBy, includeProperties, skip, take).ToListAsync();
        }

        public virtual TEntity GetOne(
            List<Expression<Func<TEntity, bool>>> filters = null,
            string includeProperties = "")
        {
            return GetQueryable(filters, null, includeProperties).SingleOrDefault();
        }

        public virtual async Task<TEntity> GetOneAsync(
            List<Expression<Func<TEntity, bool>>> filters = null,
            string includeProperties = null)
        {
            return await GetQueryable(filters, null, includeProperties).SingleOrDefaultAsync();
        }

        public virtual TEntity GetFirst(
           List<Expression<Func<TEntity, bool>>> filters = null,
           Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
           string includeProperties = "")
        {
            return GetQueryable(filters, orderBy, includeProperties).FirstOrDefault();
        }

        public virtual async Task<TEntity> GetFirstAsync(
            List<Expression<Func<TEntity, bool>>> filters = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null)
        {
            return await GetQueryable(filters, orderBy, includeProperties).FirstOrDefaultAsync();
        }

        public virtual TEntity GetById(object id)
        {
            return DbContext.Set<TEntity>().Find(id);
        }

        public virtual async Task<TEntity> GetByIdAsync(object id)
        {
            return await DbContext.Set<TEntity>().FindAsync(id);
        }

        public virtual int GetCount(List<Expression<Func<TEntity, bool>>> filters = null)
        {
            return GetQueryable(filters).Count();
        }

        public virtual async Task<int> GetCountAsync(List<Expression<Func<TEntity, bool>>> filters = null)
        {
            return await GetQueryable(filters).CountAsync();
        }

        public virtual bool GetExists(List<Expression<Func<TEntity, bool>>> filters = null)
        {
            return GetQueryable(filters).Any();
        }

        public virtual async Task<bool> GetExistsAsync(List<Expression<Func<TEntity, bool>>> filters = null)
        {
            return await GetQueryable(filters).AnyAsync();
        }

        public virtual void Create(TEntity entity)
        {
            DbContext.Set<TEntity>().Add(entity);
        }

        public virtual void Update(TEntity entity)
        {
            DbContext.Set<TEntity>().Attach(entity);
            DbContext.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(object id)
        {
            var entity = DbContext.Set<TEntity>().Find(id);
            Delete(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            if (DbContext.Entry(entity).State == EntityState.Detached)
            {
                _dbSet.Attach(entity);
            }
            _dbSet.Remove(entity);
        }
    }
}