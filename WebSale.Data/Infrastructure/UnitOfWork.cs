﻿using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;

namespace WebSale.Data.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbFactory _dbFactory;
        private WebSaleDbContext _dbContext; //= new WebSaleDbContext();

        public UnitOfWork(IDbFactory dbFactory)
        {
            this._dbFactory = dbFactory;
        }

        public WebSaleDbContext DbContext => _dbContext ?? (_dbContext = _dbFactory.Init);

        public void Commit()
        {
            DbContext.SaveChanges();
        }

        public virtual Task CommitAsync()
        {
            try
            {
                return DbContext.SaveChangesAsync();
            }
            catch (DbEntityValidationException e)
            {
                ThrowEnhancedValidationException(e);
            }

            return Task.FromResult(0);
        }

        protected virtual void ThrowEnhancedValidationException(DbEntityValidationException e)
        {
            var errorMessages = e.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

            var fullErrorMessage = string.Join("; ", errorMessages);
            var exceptionMessage = string.Concat(e.Message, " The validation errors are: ", fullErrorMessage);
            throw new DbEntityValidationException(exceptionMessage, e.EntityValidationErrors);
        }
    }
}