﻿using WebSale.Data.Infrastructure;
using WebSale.Model.Models;

namespace WebSale.Data.Repositories
{
    public interface IProductDetailRepository : IRepository<ProductDetail>
    {
    }

    public class ProductDetailRepository : RepositoryBase<ProductDetail>, IProductDetailRepository
    {
        public ProductDetailRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}