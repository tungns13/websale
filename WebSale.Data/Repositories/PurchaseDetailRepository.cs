﻿using WebSale.Data.Infrastructure;
using WebSale.Model.Models;

namespace WebSale.Data.Repositories
{
    public interface IPurchaseDetailRepository : IRepository<PurchaseDetail>
    {
    }

    public class PurchaseDetailRepository : RepositoryBase<PurchaseDetail>, IPurchaseDetailRepository
    {
        public PurchaseDetailRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}