﻿using WebSale.Data.Infrastructure;
using WebSale.Model.Models;

namespace WebSale.Data.Repositories
{
    public interface IPurchaseOrderRepository : IRepository<PurchaseOrder>
    {
    }

    public class PurchaseOrderRepository : RepositoryBase<PurchaseOrder>, IPurchaseOrderRepository
    {
        public PurchaseOrderRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}