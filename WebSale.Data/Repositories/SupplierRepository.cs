﻿using WebSale.Data.Infrastructure;
using WebSale.Model.Models;

namespace WebSale.Data.Repositories
{
    public interface ISupplierRepository : IRepository<Supplier>
    {
    }

    public class SupplierRepository : RepositoryBase<Supplier>, ISupplierRepository
    {
        public SupplierRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}