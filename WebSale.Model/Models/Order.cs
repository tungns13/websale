﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebSale.Model.Models
{
    public class Order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrderID { get; set; }

        [Required]
        public DateTime OrderDate { get; set; }

        [Required]
        public DateTime DueTime { get; set; }

        [Required]
        [ForeignKey("Customer")]
        public string CustomerID { get; set; }

        [Required]
        [ForeignKey("Employee")]
        public string EmployeeID { get; set; }

        [Required]
        public decimal SubTotal { get; set; }

        [Required]
        public decimal TaxAmt { get; set; }

        [Required]
        public decimal TotalDue { get; set; }

        [Required]
        public int Status { get; set; }

        public virtual IdentityUser Customer { get; set; }
        public virtual IdentityUser Employee { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
        public virtual ICollection<ProductDetail> ProductDetails { get; set; }
    }
}