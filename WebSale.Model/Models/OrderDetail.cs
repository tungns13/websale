﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebSale.Model.Models
{
    public class OrderDetail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrderDetailID { get; set; }

        [Required]
        [ForeignKey("Order")]
        public virtual int OrderID { get; set; }

        [Required]
        [ForeignKey("Product")]
        public virtual int ProductID { get; set; }

        [Required]
        public int TimeToWarranty { get; set; }

        [Required]
        public int OrderQty { get; set; }

        [Required]
        public decimal UnitPrice { get; set; }

        [Required]
        public int UnitPriceDiscount { get; set; }

        [Required]
        public decimal LineTotal { get; set; }

        [Required]
        public int Status { get; set; }

        public virtual Order Order { get; set; }
        public virtual Product Product { get; set; }
    }
}