﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebSale.Model.Models
{
    public class ProductDetail
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key, Column(Order = 0)]
        [ForeignKey("Product")]
        public int ProductID { get; set; }

        [Key, Column(Order = 1)]
        [Index(IsUnique = true)]
        [MaxLength(256)]
        public string ProductImei { get; set; }

        [Required]
        [ForeignKey("PurchaseOrder")]
        public int PurchaseOrderID { get; set; }

        [ForeignKey("Order")]
        public int OrderID { get; set; }

        [Required]
        public string Description { get; set; }

        public virtual Product Product { get; set; }
        public virtual PurchaseOrder PurchaseOrder { get; set; }
        public virtual Order Order { get; set; }
    }
}