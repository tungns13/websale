﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebSale.Model.Models
{
    public class PurchaseOrder
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PurchaseOrderID { get; set; }

        [Required]
        public DateTime OrderDate { get; set; }

        [Required]
        public DateTime ShipDate { get; set; }

        [Required]
        [ForeignKey("Supplier")]
        public int SupplierID { get; set; }

        [Required]
        [ForeignKey("Employee")]
        public string EmployeeID { get; set; }

        [Required]
        public decimal Subtotal { get; set; }

        [Required]
        public decimal TaxAmt { get; set; }

        [Required]
        public decimal TotalDue { get; set; }

        [Required]
        [MaxLength(256)]
        public string Comment { get; set; }

        [Required]
        public int Status { get; set; }

        public virtual Supplier Supplier { get; set; }
        public virtual IdentityUser Employee { get; set; }
    }
}