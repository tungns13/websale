﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebSale.Model.Models
{
    public class Supplier
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SupplierID { get; set; }

        [Required]
        [MaxLength(256)]
        public string CompanyName { get; set; }

        [Required]
        [MaxLength(256)]
        public string ContactName { get; set; }

        [Required]
        [MaxLength(256)]
        public string Address { get; set; }

        [Required]
        [MaxLength(24)]
        public string Phone { get; set; }

        [Required]
        [MaxLength(24)]
        public string Fax { get; set; }

        [Required]
        public string Notes { get; set; }

        [Required]
        public string HomePage { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}