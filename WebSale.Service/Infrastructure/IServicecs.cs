﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebSale.Service
{
    public interface IServicecs<TEntity>
        where TEntity : class
    {
        IEnumerable<TEntity> Get(int? page, int? pageSize, Dictionary<string, string> filters, string orderBy, bool desc = false);

        Task<IEnumerable<TEntity>> GetAsync(int? page, int? pageSize, Dictionary<string, string> filters, string orderB, bool desc = false);

        TEntity GetDetail(int id);

        Task<TEntity> GetDetailAsync(int id);

        void Add(TEntity entity);

        Task AddAsync(TEntity entity);

        void Update(TEntity entity);

        Task UpdateAsync(TEntity entity);

        int GetCount(Dictionary<string, string> filters);

        Task<int> GetCountAsync(Dictionary<string, string> filters);

        void Delete(int id);

        Task DeleteAsync(int id);
    }
}