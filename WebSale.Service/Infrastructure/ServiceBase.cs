﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WebSale.Data.Infrastructure;

namespace WebSale.Service.Infrastructure
{
    public abstract class ServiceBase<TEntity> : IServicecs<TEntity>
        where TEntity : class
    {
        private readonly IRepository<TEntity> _repository;
        protected readonly IUnitOfWork UnitOfWork;

        protected ServiceBase(IRepository<TEntity> repository, IUnitOfWork unitOfWork)
        {
            _repository = repository;
            UnitOfWork = unitOfWork;
        }

        public void Add(TEntity entity)
        {
            _repository.Create(entity);
            UnitOfWork.Commit();
        }

        public Task AddAsync(TEntity entity)
        {
            _repository.Create(entity);
            return UnitOfWork.CommitAsync();
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
            UnitOfWork.Commit();
        }

        public Task DeleteAsync(int id)
        {
            _repository.Delete(id);
            return UnitOfWork.CommitAsync();
        }

        protected IEnumerable<TEntity> GetBase(int? page, int? pageSize, List<Expression<Func<TEntity, bool>>> filters, string orderBy, bool desc)
        {
            if (orderBy == null)
            {
                return _repository.Get(filters, null, null, null, null);
            }
            var order = BuildOrder(orderBy, desc);
            return _repository.Get(filters, order, null, page * pageSize, pageSize);
        }

        protected async Task<IEnumerable<TEntity>> GetBaseAsync(int? page, int? pageSize, List<Expression<Func<TEntity, bool>>> filters, string orderBy, bool desc)
        {
            if (orderBy == null)
            {
                return await _repository.GetAsync(filters, null, null, null, null);
            }
            var order = BuildOrder(orderBy, desc);
            return await _repository.GetAsync(filters, order, null, page * pageSize, pageSize);
        }

        public virtual IEnumerable<TEntity> Get(int? page, int? pageSize, Dictionary<string, string> filters = null, string orderBy = null, bool desc = false)
        {
            return GetBase(page, pageSize, BuildFilterExpressions(filters), orderBy, desc);
        }

        public virtual async Task<IEnumerable<TEntity>> GetAsync(int? page = null, int? pageSize = null, Dictionary<string, string> filters = null, string orderBy = null, bool desc = false)
        {
            return await GetBaseAsync(page, pageSize, BuildFilterExpressions(filters), orderBy, desc);
        }

        public int GetCount(Dictionary<string, string> filters)
        {
            var filtersExpression = new List<Expression<Func<TEntity, bool>>>();
            if (filters == null) return _repository.GetCount(filtersExpression);
            foreach (var filter in filters)
            {
                filtersExpression.Add(entity => entity.GetType().GetProperty(filter.Key).GetValue(entity, null).ToString().Contains(filter.Value));
            }
            return _repository.GetCount(filtersExpression);
        }

        public async Task<int> GetCountAsync(Dictionary<string, string> filters)
        {
            var filtersExpression = new List<Expression<Func<TEntity, bool>>>();
            if (filters == null) return await _repository.GetCountAsync(filtersExpression);
            foreach (var filter in filters)
            {
                filtersExpression.Add(entity => entity.GetType().GetProperty(filter.Key).GetValue(entity, null).ToString().Contains(filter.Value));
            }
            return await _repository.GetCountAsync(filtersExpression);
        }

        public TEntity GetDetail(int id)
        {
            return _repository.GetById(id);
        }

        public async Task<TEntity> GetDetailAsync(int id)
        {
            return await _repository.GetByIdAsync(id);
        }

        public void Update(TEntity entity)
        {
            _repository.Create(entity);
            UnitOfWork.Commit();
        }

        public Task UpdateAsync(TEntity entity)
        {
            _repository.Create(entity);
            return UnitOfWork.CommitAsync();
        }

        public List<Expression<Func<TEntity, bool>>> BuildFilterExpressions(Dictionary<string, string> filters)
        {
            if (filters == null)
            {
                return null;
            }
            var filtersExpression = new List<Expression<Func<TEntity, bool>>>();
            Expression<Func<TEntity, bool>> expression;
            foreach (var filter in filters)
            {
                //if filter by foreign key
                if (filter.Key.ToLower().Contains("id"))
                {
                    expression = System.Linq.Dynamic.DynamicExpression.ParseLambda<TEntity, bool>(
                    filter.Key + " = @0", int.Parse(filter.Value));
                }
                else
                {
                    expression = System.Linq.Dynamic.DynamicExpression.ParseLambda<TEntity, bool>(
                    filter.Key + ".Contains(@0)", filter.Value);
                }
                filtersExpression.Add(expression);
            }
            return filtersExpression;
        }

        public Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> BuildOrder(string orderBy, bool desc)
        {
            if (desc)
            {
                return (data => data.OrderByDescending(entity => entity.GetType().GetProperty(orderBy)));
            }
            else
            {
                return (data => data.OrderBy(entity => entity.GetType().GetProperty(orderBy)));
            }
        }
    }
}