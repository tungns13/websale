﻿using WebSale.Data.Infrastructure;
using WebSale.Model.Models;
using WebSale.Service.Infrastructure;

namespace WebSale.Service.Services
{
    public interface IOrderDetailService : IServicecs<OrderDetail>
    {
    }

    public class OrderDetailService : ServiceBase<OrderDetail>, IOrderDetailService
    {
        public OrderDetailService(IRepository<OrderDetail> repository, IUnitOfWork unitOfWork) : base(repository, unitOfWork)
        {
        }
    }
}