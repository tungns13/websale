﻿using WebSale.Data.Infrastructure;
using WebSale.Model.Models;
using WebSale.Service.Infrastructure;

namespace WebSale.Service.Services
{
    public interface IOrderService : IServicecs<Order>
    {
    }

    public class OrderService : ServiceBase<Order>, IOrderService
    {
        public OrderService(IRepository<Order> repository, IUnitOfWork unitOfWork) : base(repository, unitOfWork)
        {
        }
    }
}