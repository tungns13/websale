﻿using WebSale.Data.Infrastructure;
using WebSale.Model.Models;
using WebSale.Service.Infrastructure;

namespace WebSale.Service.Services
{
    public interface IProductDetailService : IServicecs<ProductDetail>
    {
    }

    public class ProductDetailService : ServiceBase<ProductDetail>, IProductDetailService
    {
        public ProductDetailService(IRepository<ProductDetail> repository, IUnitOfWork unitOfWork) : base(repository, unitOfWork)
        {
        }
    }
}