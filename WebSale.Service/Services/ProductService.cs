﻿using WebSale.Data.Infrastructure;
using WebSale.Model.Models;
using WebSale.Service.Infrastructure;

namespace WebSale.Service.Services
{
    public interface IProductService : IServicecs<Product>
    {
    }

    public class ProductService : ServiceBase<Product>, IProductService
    {
        public ProductService(IRepository<Product> repository, IUnitOfWork unitOfWork) : base(repository, unitOfWork)
        {
        }
    }
}