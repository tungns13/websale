﻿using WebSale.Data.Infrastructure;
using WebSale.Model.Models;
using WebSale.Service.Infrastructure;

namespace WebSale.Service.Services
{
    public interface IPurchaseDetailService : IServicecs<PurchaseDetail>
    {
    }

    public class PurchaseDetailService : ServiceBase<PurchaseDetail>, IPurchaseDetailService
    {
        public PurchaseDetailService(IRepository<PurchaseDetail> repository, IUnitOfWork unitOfWork) : base(repository, unitOfWork)
        {
        }
    }
}