﻿using WebSale.Data.Infrastructure;
using WebSale.Model.Models;
using WebSale.Service.Infrastructure;

namespace WebSale.Service.Services
{
    public interface IPurchaseOrderService : IServicecs<PurchaseOrder>
    {
    }

    public class PurchaseOrderService : ServiceBase<PurchaseOrder>, IPurchaseOrderService
    {
        public PurchaseOrderService(IRepository<PurchaseOrder> repository, IUnitOfWork unitOfWork) : base(repository, unitOfWork)
        {
        }
    }
}