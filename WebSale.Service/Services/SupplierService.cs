﻿using WebSale.Data.Infrastructure;
using WebSale.Model.Models;
using WebSale.Service.Infrastructure;

namespace WebSale.Service.Services
{
    public interface ISupplierService : IServicecs<Supplier>
    {
    }

    public class SupplierService : ServiceBase<Supplier>, ISupplierService
    {
        public SupplierService(IRepository<Supplier> repository, IUnitOfWork unitOfWork) : base(repository, unitOfWork)
        {
        }
    }
}