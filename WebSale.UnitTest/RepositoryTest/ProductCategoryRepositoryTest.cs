﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using WebSale.Data.Infrastructure;
using WebSale.Data.Repositories;
using WebSale.Model.Models;

namespace WebSale.UnitTest.RepositoryTest
{
    [TestClass]
    public class PostCategoryRepositoryTest
    {
        private IDbFactory _dbFactory;
        private ICategoryRepository _repository;
        private IUnitOfWork _unitOfWork;

        [TestInitialize]
        public void Initialize()
        {
            _dbFactory = new DbFactory();
            _repository = new CategoryRepository(_dbFactory);
            _unitOfWork = new UnitOfWork(_dbFactory);
        }

        [TestMethod]
        public void PostCategory_Repository_Create()
        {
            var category = new Category
            {
                CategoryId = 1,
                CategoryName = "CategoryTest1"
            };

            _repository.Create(category);
            _unitOfWork.Commit();

            var list = _repository.GetAll().ToList();

            Assert.AreEqual(1, list.Count);
        }
    }
}