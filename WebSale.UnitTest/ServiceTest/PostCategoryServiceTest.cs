﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using WebSale.Data.Infrastructure;
using WebSale.Data.Repositories;
using WebSale.Model.Models;
using WebSale.Service.Services;

namespace WebSale.UnitTest.ServiceTest
{
    [TestClass]
    public class PostCategoryServiceTest
    {
        private Mock<ICategoryRepository> _mockRepository;
        private Mock<IUnitOfWork> _mockUnitOfWork;
        private ICategoryService _categoryService;
        private List<Category> _listCategory;

        [TestInitialize]
        public void Initialize()
        {
            _mockRepository = new Mock<ICategoryRepository>();
            _mockUnitOfWork = new Mock<IUnitOfWork>();
            _categoryService = new CategoryService(_mockRepository.Object, _mockUnitOfWork.Object);
            _listCategory = new List<Category>
            {
                new Category {CategoryId =1 ,CategoryName= "Category1" },
                new Category {CategoryId =2 ,CategoryName= "Category2" },
                new Category {CategoryId = 3 ,CategoryName= "Category3"}
            };
        }

        [TestMethod]
        public void PostCategory_Service_GetAll()
        {
            _mockRepository.Setup(repo => repo.Get(null, null, null, null, null)).Returns(_listCategory);

            var result = _categoryService.Get(null, null, null, null, false) as List<Category>;

            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
        }

        [TestMethod]
        public void PostCategory_Service_Create()
        {
            var category = new Category
            {
                CategoryId = 1,
                CategoryName = "Category1"
            };
            _categoryService.Add(category);
        }
    }
}