﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WebSale.Data.Infrastructure;
using WebSale.Data.Repositories;
using WebSale.Service.Services;

namespace WebSale.Web.API
{
    public class CategoryController : ApiController
    {
        private CategoryService _service;

        private CategoryService Service
        {
            get
            {
                if (_service == null)
                {
                    _service = new CategoryService(new CategoryRepository(new DbFactory()), new UnitOfWork(new DbFactory()));
                }
                return _service;
            }
            set
            {
                _service = value;
            }
        }

        // GET api/<controller>
        [HttpPost]
        public async Task<IEnumerable<Model.Models.Category>> GetAsync(Dictionary<string, string> filters)
        {
            return await Service.GetAsync(null, null, filters);
        }

        [HttpPost]
        public IEnumerable<Model.Models.Category> Get(Dictionary<string, string> filters)
        {
            return Service.Get(null, null, filters);
        }

        // GET api/<controller>/5
        public Model.Models.Category GetDetail(int id)
        {
            return Service.GetDetail(id);
        }

        // POST api/<controller>
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
    }
}